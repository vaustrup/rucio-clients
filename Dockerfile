ARG TAG

FROM rucio/rucio-clients:release-34.4.3

USER root

COPY --chown=user:user rucio.cfg /opt/rucio/etc/
COPY ca.repo /etc/yum.repos.d/

COPY EGI-trustanchors.repo /etc/yum.repos.d/
RUN yum -y install ca-policy-egi-core

RUN yum -y install http://linuxsoft.cern.ch/wlcg/el9/x86_64/wlcg-repo-1.0.0-1.el9.noarch.rpm && \
    yum -y install CERN-CA-certs ca-policy-lcg ca-policy-egi-core wlcg-voms-atlas && \
    yum clean all && \
    rm -rf /var/cache/yum

ENV AUTH_TYPE=x509_proxy

USER user

CMD ["/bin/bash"]
